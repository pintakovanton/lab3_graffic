package main;

import Jama.Matrix;
import util.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Model {

    private final static double T_STEP = 0.1;
    private final static double W_STEP = 0.1;

    private final static double[][] curvesMatrix = {
            {-1, 3,-3, 1},
            {3, -6, 3, 0},
            {-3, 0, 3, 0},
            {1,4, 1, 0}



    };




    public static List<List<Point3D>> plotBSpline(final List<Point3D> basePoints) {


        double[][] xMatrix = new double[4][4];
        double[][] yMatrix = new double[4][4];
        double[][] zMatrix = new double[4][4];

        for (int r = 0; r < 4; r++) {
            for (int c = 0; c < 4; c++) {
                xMatrix[r][c] = basePoints.get(r * 4 + c).getX();
                yMatrix[r][c] = basePoints.get(r * 4 + c).getY();
                zMatrix[r][c] = basePoints.get(r * 4 + c).getZ();
            }
        }

        Matrix basis = new Matrix(curvesMatrix) ;
        basis = basis.times(0.2);
        Matrix preparedXMatrix = (basis.times(new Matrix(xMatrix))).times(basis.transpose());
        Matrix preparedYMatrix = ( basis.times(new Matrix(yMatrix))).times(basis.transpose());
        Matrix preparedZMatrix = (basis.times(new Matrix(zMatrix))).times(basis.transpose());

        double[][] tTempMatrix = new double[1][4];
        double[][] wTempMatrix = new double[4][1];

        List<List<Point3D>> linesOfSurface = new ArrayList<List<Point3D>>();

        for (double t = 0; t <= 1.0; t += T_STEP) {

            for (int i = 0; i <= 3; i++) {
                tTempMatrix[0][i] = Math.pow(t, 3 - i);
            }
            List<Point3D> currLine = new ArrayList<Point3D>();
            Matrix tMatrix = new Matrix(tTempMatrix);
            for (double w = 0; w <= 1.0; w += W_STEP) {

                for (int i = 0; i <= 3; i++) {
                    wTempMatrix[i][0] = Math.pow(w, 3 - i);
                }
                Matrix wMatrix = new Matrix(wTempMatrix);

                double x = (tMatrix.times(preparedXMatrix)).times(wMatrix).get(0, 0);
                double y = (tMatrix.times(preparedYMatrix)).times(wMatrix).get(0, 0);
                double z = (tMatrix.times(preparedZMatrix)).times(wMatrix).get(0, 0);

                currLine.add(new Point3D(x, y, z));
            }
            linesOfSurface.add(currLine);
        }
        return linesOfSurface;
    }


}